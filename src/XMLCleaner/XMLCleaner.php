<?php


namespace Dracoder\XMLCleaner;


class XMLCleaner
{
    /**
     * @param string $xmlDocument
     *
     * @return string
     */
    public function cleanDocument(string $xmlDocument): string
    {
        return preg_replace('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u',
            ' ', $xmlDocument);

    }

    /**
     * @param string $value
     *
     * @return string
     */
    public function cleanValue(string $value): string
    {
        $cleanedXMLValue = str_replace('&', '&amp;', $value);
        $cleanedXMLValue = str_replace('<', '&lt;', $cleanedXMLValue);
        $cleanedXMLValue = str_replace('>', '&gt;', $cleanedXMLValue);
        $cleanedXMLValue = str_replace('"', '&quot;', $cleanedXMLValue);
        $cleanedXMLValue = str_replace("'", '&apos;', $cleanedXMLValue);

        return $cleanedXMLValue;
    }

}